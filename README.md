# DarkAge v0.1

A multiplayer real-time (fast-paced) online game consists of clients that follow the instructions 
of an authoritative server over a network in clever ways to mitigate lag.

## Architecture
The code follows a server-client model.
Game logic (mostly physics) is run on both server and client, however,
the server is the source of truth for the state of the World. 
The client tries to be in sync with the true worldstate using the snapshots from the 
server plus the input made by the players.

Initial connection via express + PUG templates to prompt hero selection.

After they choose a hero, a socket connection is established. 
Afterwards the server sends snapshots of true worldstate to the client at regular intervals.
The client runs the game locally and uses those snapshots to approximate the true worldstate as best it can using one or more of the following strategies:

1. Client-side prediction and server reconciliation
2. Entity interpolation
3. Lag compensation

The selection and  particular implementation of those depends on the needs of the game.

## Server, entrypoint app.js + sockets.js

Two main loops.

- Physics loop (Worldstate loop) each 15 ms ~ 66 updates/s. 66 Hz.
Determine the true unique Worldstate snapshot at time t+1 from time t using all actions from all actors
plus worlstate at time t.

Solution:

1. Gather and process player actions, aij.
2. From worldstate W_t and aij, determine W_t+1 (game engine step)
3. Store consequences of actions (results or W_t+1) and clear aij.


- Broadcast loop, 45ms ~ 22 updates/s. 22 hz.
1. For each client i
    - Gather all relevant info, send true worldstate W_t
2. Repeat

This can be made more efficient for huge worlds by only sending the required info for each player i.e.
a subset of the worldstate w\subset W. w_t.


## Client, entrypoint client.js
Two main loops.

1. Local worldstate w' loop, at 15ms ~ 66 updates/s. 66 Hz.

Keep positions in sync with the server true position. (Sync local state to true state.)

Approximate w'_t ~ w_t assuming physics is deterministic.

In other words, attempts to mask network delay for the client.

This loop can be as complex as needed


2. Render loop (as fast as possible (requestAnimationFrame)) ~ 60 fps

Display user in the present and entities in the past.

1. Clear canvas
2. Draw meta-info.
4. Update player worldview w' using one or more of the following techniques
    - Client side prediction and Server reconciliation
    - Entity interpolation
    - Lag compensation
5. Move other entities based on true server 
6. Draw other players in canvas

It needs to handle local input and send to server as soon as it can or in chunks at
specific intervals dtc.


## Desiderata for v0.1
- [x] frame-rate is device (hardware) independent. (solved with requestAnimationFrame)
- [x] db to save heroes. (solved with postgreSQL)
- [ ] client-side interpolation.
- [x] create multiple heros for a player. (solved with sessions)
- [x] use cache to keep track of players and games (worlds) (solved with redis)
- [ ] monsters
- [ ] items (bag + equipment)
- [ ] simple combat with missiles

## todo@ project IMPROVEMENTS
- [ ] getUserFromSession put it as callback in post routes http://expressjs.com/en/api.html#middleware-callback-function-examples, so it returns as req.user.
- [ ] typed arrays to and from the server instead of json to lower package sizes. minimize data going over the wire.
- [ ] encrypt sprites?
- [ ] security, snyk auth, snyk test, npm audit  https://expressjs.com/en/advanced/best-practice-security.html#use-cookies-securely
- [ ] use nonce to enter the world from Hero select screen
- [ ] minify and mangle code sent to the client


## todo@project Lag compensation mechanisms
S: server, aijk: actions j from client i at time k, G: game engine
Wt true worldstate at time 't', Ci: client i.
w_t local true worldstate
w'_t local approximate worldstate (client's view)

S gets aij with timestamps
G processes aijk to obtain Wt
S sends Wt at constant dts.

Ci sends aij and simulates w'_t
Ci gets updates for w_t
-> sync w'_t to w_t for his own state?
-> interpolates past w_t-j for all other entities

Best solution:
G can determine W_t+1 by reconstruction of w'_t for every client's POV.
It can know what w'_t looked liked i.e. past positions of enemies but for Ci it was their present.