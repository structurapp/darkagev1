const express = require('express')
const Router = require('express-promise-router')
const router = new Router()
const {check, validationResult} = require('express-validator')
var path = require('path')
const {v4: uuidv4} = require('uuid')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const { auth } = require('express-openid-connect');
const { requiresAuth } = require('express-openid-connect');
const { nextTick } = require('process')
const { signedCookies, signedCookie } = require('cookie-parser')
const pug = require('pug')
const { update } = require('lodash')

var app = express()
var server = require('http').createServer(app)
var io = require('socket.io')(server, {});

const { redisClient: redis, getUserIDFromSession } = require('./server/redis.js')
const { saveUserDB, getUserDB, dbpool: db } = require('./server/db.js')


const { AUTH0_SECRET, 
    AUTH0_CLIENT_ID,
    HMAC_KEY,
    BASE_URL,
    DEBUG,
    PORT,
    APP_SECURE,
} = require('./server/config.js')


const HMACkey = HMAC_KEY // openssl rand -base64 [bytes] -> 32 = 256 bits  encoded. todo@project -> env variables!
const secure = APP_SECURE //   secure: true // todo@project enable in production  !
const session_id = 'session_id'
const appSessionCookieLength = 64

const config = {
    authRequired: false,
    auth0Logout: true,
    secret: AUTH0_SECRET,
    baseURL: BASE_URL + ':' + PORT,
    clientID: AUTH0_CLIENT_ID,
    issuerBaseURL: 'https://byll.auth0.com',
    routes: {
    // auth router attaches /login, /logout, and /callback routes to the baseURL
        callback: '/logged_in_redirect',
    }
};
// prevent against casual express-targeted exploits
app.disable('x-powered-by')
// first use oauth to check whether user has logged in
app.use(auth(config));
// if not logged in then redirect to anonymous session.
app.use(cookieParser(HMACkey))
// parses request.body in json (posts from the frontend or other apis)
app.use(bodyParser.json())
// for parsiing application/xwww-form-urlencoded forms.
app.use(bodyParser.urlencoded({ extended: true })); 
// serves everything from the client from public folder
app.use(express.static(__dirname + '/public'))
// pug config.
app.set('view engine', 'pug')

app.get('/', async (req, res, next) => {
    let isAuthed = req.oidc.isAuthenticated()
    if (isAuthed) {
        // user logged in via oauth, cookie name appSession and req.oicd.user populated with session info.
        try {
            var user
            var heroes
            var result
            let email = req.oidc.user.email

            result = await db.query(
                'SELECT id, name, picture FROM users WHERE email = $1;',
                [email]
            )
            
            if (result.rows.length == 0) {
                console.log(`\n New logged in user! ${email}.\n`)
                let picture = req.oidc.user.picture
                let nickname = req.oidc.user.nickname
                let locale = req.oidc.user.locale.rows
                let email_verified = req.oidc.user.email_verified
                let updated_at = req.oidc.user.updated_at
                let sub = req.oidc.user.sub
                let name = req.oidc.user.name
                result = await db.query(
                    'INSERT INTO users(email, name, picture, nickname, locale, email_verified, sub, updated_at, anon)' + 
                    'VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING id, name, picture;',
                    [email, name, picture, nickname, locale, email_verified, sub, updated_at, false]
                )
            }

            user = result.rows[0]

            heroResult = await db.query('SELECT * FROM heroes where user_id = $1;', [user.id])
            heroes = heroResult.rows

            const headers = req.headers
            const ip = req.ip
            const cookies = req.headers.cookie
            const unparsedCookies = decodeURIComponent(cookies)
            const parsedCookies = require('cookie').parse(unparsedCookies)
            // console.log('parsed cookies', parsedCookies)
            const appSession = parsedCookies['appSession'].substring(0, appSessionCookieLength)
            // console.log('saving session', appSession.length)

            redis.set(appSession, user.id, 'EX', 60 * 60, (err, reply) => {})

            // save their web_session, we could do single/multiple session enforcement here.
            await db.query(
                'INSERT INTO sessions(user_id, active, ip, user_agent, session_cookie) VALUES($1, $2, $3, $4, $5);',
                [user.id, true, ip, JSON.stringify(headers['user-agent']), appSession]
            )

            res.render('heroes', {
                name: user.name,
                heroes: heroes,
                anon: user.anon,
                errors: []
            })

        } catch(err) {
            console.log(err)
            errorMsg = JSON.stringify(err)
            res.send('Something went awfully wrong on first login (authed) ' + errorMsg)
        }

    } else {
        // pass to anon handler.
        next()
    }
});

// anon user handler.
app.get('/', async (req, res) => {
    const headers = req.headers
    const ip = req.ip
    const signedCookies = req.signedCookies
    var session_cookie = signedCookies[session_id]
    var user
    var heroes
    var result
    try {
        if (session_cookie) {
            console.log('Subsequent visit: ' + session_cookie + '\n')
            result = await db.query('SELECT id, name, email, picture, anon FROM users WHERE "email" = $1;', [session_cookie])
        } else {
            console.log('FIRST VISIT!\n')

            session_cookie  = uuidv4() 
            res.cookie(session_id, session_cookie, {
                maxAge: 1000*60*60*24*365*10, // set cookie to 10y... 10y to create an account.
                httpOnly: true,
                signed: true, // uses HMAC-SHA256 to encrypt session_cookie (overkill.)
                secure: secure,
            }) 
            result = await db.query(
                'INSERT INTO users(email, name, anon) VALUES($1, $2, $3) ' + 
                'RETURNING id, name, anon;',
                [session_cookie, 'Anonymous', true]
            )
        }
        user = result.rows[0]

        result = await db.query(
            'SELECT * FROM heroes where user_id = $1 and playing = $2;', 
            [user.id, false])
        
        heroes = result.rows

        // save their web_session, we can do single/multile session enforcement here.
        const { rows: sessionRows } = await db.query(
            'INSERT INTO sessions(user_id, active, ip, user_agent, session_cookie) VALUES($1, $2, $3, $4, $5);',
            [user.id, true, ip, JSON.stringify(headers['user-agent']), session_cookie]
        ) 
        redis.set(session_cookie, user.id, 'EX', 60 * 60, (err, reply) => {})

    } catch (err) {
        console.error(err)
        errorMsg = JSON.stringify(err)
        res.send('Something went awfully wrong (anon) \n' + errorMsg)
    }

    // once we have logged them in anonymously and saved their session,
    // redirect them to hero selection page
    res.render('heroes', {
        name: user.name,
        heroes: heroes,
        anon: true,
        errors: []
    })
})



app.get('/index', (req, res) => {
    console.log('navigating to index! request params', req.params)
    res.sendFile(path.join(__dirname, 'index.html'))
})

const getUserFromCookies = (cookies) => {
    // tries to grab a user object from the db using only the cookie
    // it will first try to grab user_id from redis and then query the db
    // for the whole object, otherwise it must query the session and do a join.
    let unparsedCookies = decodeURIComponent(cookies)
    let parsedCookies = require('cookie').parse(unparsedCookies)
    let keys = ['appSession', session_id]

    let found
    for (let k of keys) {
        if (parsedCookies.hasOwnProperty(k)) {
            found = true
            break
        }
    }

    if (!found) {
        throw new Error(`No cookies found when searching for user, are you logged in? ${JSON.stringify(parsedCookies)}`)
    }

    var session_cookie = parsedCookies['appSession']

    if (session_cookie) {
        session_cookie = session_cookie.substring(0, appSessionCookieLength)
    } else {
        session_cookie = cookieParser.signedCookie(parsedCookies[session_id], HMACkey)

    }

    let promise = new Promise((resolve, reject) => {
        getUserIDFromSession(session_cookie).then(r => {
            if (r) {
                    db.query('SELECT id, name, email, picture, anon FROM users where id = $1;', [parseInt(r)])
                    .then(result => {
                        let user = result.rows[0]
                        resolve(user)
                    })
                    .catch(e => reject(e))
            }
            else {
                db.query('SELECT user_id FROM sessions WHERE session_cookie = $1;', [session_cookie])
                        .then(result => {
                            if (result.rows == 0) {
                                reject('No user_id found for session! Review.')
                            }
                            let session = result.rows[0]
                            // console.log('session', result.rows)
                            db.query('SELECT id, name, email, picture, anon FROM users where id = $1;', [session.user_id])
                            .then(result => {
                                let user = result.rows[0]
                                resolve(user)
                            })
                            .catch(e => reject(e))
                        })
                        .catch(e => reject(e))
            }
        }).catch(e => reject(e))
    })

    return promise
}

app.post('/createHero', 
    [
        // https://express-validator.github.io/docs/index.html
        // todo@hero-selection todo@carlo todo@project validate names/races at the form and db level. 
        // check('name')
        // .isLength({min: 1})
        // .withMessage('Please enter a name'),
        // check('race')
        // .isLength({min: 1})
        // .withMessage('Please enter a valid race (human, orc)'),

    ],
    async (req, res) => {

        var heroName = req.body.heroName
        var heroRace = req.body.heroRace
        var user

        try {
            user = await getUserFromCookies(req.headers.cookie)
            let sprite = [380, 602, 26, 38]
            console.log('user inside createhero', user)
            const result = await db.query('INSERT INTO heroes (user_id, name, race, sprite) VALUES($1, $2, $3, $4) ' +
                                          'RETURNING id;', [user.id, heroName, heroRace, sprite]
                                         )
                                            
            const hero_id = result.rows[0].id
            res.redirect('/index?hero=' + hero_id)
        } catch(err) {
            console.log(err)
            if (err.detail.includes('already exists')) {
                const {rows: heroes} = await db.query('SELECT * FROM heroes where user_id = $1 and playing = $2;', [user.id, false])
                res.render('heroes', {
                    name: user.name,
                    anon: user.anon,
                    heroes: heroes,
                    errors: [`A hero with name "${heroName}" was already created for this account.`]
                })
            }
            throw new Error('Something went wrong while creating hero' + JSON.stringify(err))
        }
    }
)


server.listen(PORT, () => {
    console.log(`\n\n Listening on *:${PORT}. \n\n`)
})

module.exports = { server, io, getUserFromCookies }
const SOCKETS = require('./sockets.js')