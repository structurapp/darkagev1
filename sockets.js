const { saveUserDB, getUserDB, dbpool: db } = require('./server/db.js')
const { redisClient: redis, getUserIDFromSession } = require('./server/redis.js')
const { io, getUserFromCookies } = require('./app.js')

const heroModule = require('./public/hero.js')
const HeroFactory = heroModule.HeroFactory

const { processHeroActions, EngineTimeStep } = require('./server/engine.js')
const { DEBUG } = require('./server/config.js')

const { isEmpty }  = require('lodash')

const BROADCAST_INTERVAL = 1000 // 22 ms
const WORLDSTATE_INTERVAL = 500 //15 ms
let SOCKETS_BY_HERO = {}
// todo@project is there a way to get async on sockets?
// -- HANDLE CONNECTIONS by clients
io.sockets.on('connection', socket => {
    console.info('CLIENT CONNECTED.')
    // we can verify nonce here to make sure client navigated directly from hero selection screen if !playing!
    const params = socket.handshake.query.params
    const hero_id = parseInt(params.split('=')[1])
    const cookies = socket.handshake.headers.cookie
    var hero
    getUserFromCookies(cookies)
    .then(user => {
        db.query('SELECT * FROM heroes where user_id = $1 and id = $2;', [user.id, hero_id])
        .then(r => {
            if (r.rows.length > 0) {
                // console.log('Passed into hero factory', r.rows[0])
                hero = HeroFactory(r.rows[0])
                socket.hero = hero // stick all relevant properties on the socket.
                SOCKETS_BY_HERO[hero.id] = socket
                socket.actions = []

                redis.sadd('players', hero.id)
                redis.smembers('players', (req, onlineHeroIDs) => {
                    socket.players = onlineHeroIDs
                })
                db.query('UPDATE heroes SET playing = $3 WHERE user_id = $1 AND id = $2',
                 [user.id, hero.id, true])
                .then()
                .catch(e => {
                    console.error(e)
                })

                console.log(`Hero ${hero.name} connected (id ${hero.id}).`)
                // socket.emit('snapshot', {self: hero})
                socket.emit('enterWorld', {hero: hero})
            } else {
                // todo@project return them to homescreen to login or select hero again.
                socket.close()
                throw new Error('Hero not from this user!, review and fix.')
            }
        })
        .catch(e => {
            console.error(e)
        })
    })
    .catch(e => console.log(e))

    socket.on('action', actions => {

        if (!isEmpty(actions) && !isEmpty(socket.hero)) {
            socket.actions = actions
        }
        // if (Objectsocket.hero && actions && actions.length !== 0) {
        //     // not sure why this can trigger before they connect properly.
        //     console.log('Actions by ts', socket.hero.id)
        //     // let ts = Date.now()
        //     socket.actions_by_ts[ts] = actions
        //     // socket.actions_by_ts[ts] = actions
        // }
    })

    socket.on('disconnect', e => {
        let hero_id = socket.hero.id
        console.log(`Hero ${hero_id} ${socket.hero.name} disconnected.`)
        redis.srem('players', hero_id)
        db.query('UPDATE heroes SET playing = $3 WHERE user_id = $1 AND id = $2', [socket.hero.user_id, hero_id, false])
        .then()
        .catch(e => {
            console.error(e)
        })
        delete SOCKETS_BY_HERO[hero_id]
    })
})



// Main game engine loop.
setInterval(() => {
    for (let hero_id in SOCKETS_BY_HERO) {
        let socket = SOCKETS_BY_HERO[hero_id]
        let actions = socket.actions
        let hero = socket.hero
        if (!isEmpty(hero) && !isEmpty(actions)) {
            processHeroActions(hero, actions)
            actions.length = 0
        }
    }

}, WORLDSTATE_INTERVAL)


// WORLD STATE BROADCAST LOOP
// to each player their own true local worldstate W
setInterval(() => {
    let heroes = []

    for (let hero_id in SOCKETS_BY_HERO) {
        socket = SOCKETS_BY_HERO[hero_id]
        heroes.push(socket.hero)
    }

    for (let hero_id in SOCKETS_BY_HERO) {
        let otherHeroes = heroes.filter((h) => h.id != hero_id)

        socket = SOCKETS_BY_HERO[hero_id]

        let localWorldstate =  {
            debug: DEBUG,
            serverHero: socket.hero,
            // proccesedActions: processedActions,
            heroes: otherHeroes, 
            mobs: [],
            npcs: [],
            items: [],
            tiles: [],
            missiles: [],
        }

        socket.emit('snapshot', localWorldstate)
    }

    // keep important global stuff in redis.
    redis.smembers('players', (req, onlineHeroIDs) => {
        console.log('Online heroes: ', onlineHeroIDs)
    })

}, BROADCAST_INTERVAL);

/*

responsibilities:
- 
- process client's (inputs + state) to determine next state
- emit state to clients
- store and retrieve necessary info from cache and db.

1. sample clock to find start time
2. read all client's input
3. step through the input and the server state to determine a world snapshot (simulate) (this is the game engine)
(simulate state using simulation time)
4. broadcast state to players
5. sample clock to find end time
6. (end - start) simulation time for next frame


*/