// The game Engine is a function that determines the Worldstate W at time t+1
// when given state at time t and a set of (potential) actions A in 
// regular intervals dt called 'game time steps'.
// 
// In general, the types W and A can be described as follows
//
// W: a finite set of all 'entities' that exist in the world, 
// 'exist' is an ambiguous term, though in this context it refers
// to all objects that interact with the World.
// a subset of the values in Database + cache + objects in memory.
// Potential Actions A:
// Client i can execute 'action' j at time k
// Therefore Aijk is the action j taken by client i at discrete timestamp k
// a tensor of values

// The engine will 'simulate' the interactions between Aijk and W_t to compute W_t+1.
// the particular implementation details are left out, this it the 'game logic'

// In general it will however not process all Aijk, but will rather return a 
// subset of those which it has processed.

// Engine <W_t, Aijk> -> W_t+1, aijk
// where aijk is a subset of Aijk

const actionsModule = require('../public/actions.js')
const genericAction = actionsModule.genericAction
const MoveAction = actionsModule.MoveAction

const EngineTimeStep = (Worldstate, Actions) => {
    // actions can be a seq. of ActionObjects with timestamps.
    // Engine has access to all objects + database
    // Apply all Actions and check at the end.
    // everything here happens in an 'instant' so we process all actions if they make sense

    let NewWorldstate = {}
    let ActionsProcessed = {}

    return NewWorldstate, ActionsProcessed
}


const processHeroActions = (hero, actions) => {
    for (let action of actions) {
        console.log('Processing action ', action)
        console.log('hero id ', hero.id)
        if ('wasd'.indexOf(action) !== -1) MoveAction(hero, action)
    }
}


// MOVE id TOWARDS x,y 
// MOVE id TO x,y
// 
module.exports = { EngineTimeStep, processHeroActions }
