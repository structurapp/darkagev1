//

const {
    DB_NAME,
    DB_HOST,
    DB_PORT,
    DB_USER
} = require('./config.js')

const Pool = require('pg').Pool
const dbpool = new Pool({
    user: DB_USER,
    host: DB_HOST,
    database: DB_NAME,
    port: DB_PORT,
})

// console.log(dbpool)

const saveUserQuery = ({name='anonymous', email}) => {
    `INSERT INTO users ("name", "email") values (${name}, ${email});`
}

const getUserQuery = ({email}) => {
    `SELECT FROM "users" where "email" = ${email};`
}
const getUserDB = (...user) => dbpool.query(getUserQuery(...user))

const saveUserDB = (...user) => dbpool.query(saveUserQuery(...user))

// const queryUser = (req, res) => {
//     dbpool.query(
//          userQuery, (err, results) => {
//             if (err) throw err

//             response.status()
//         }
//     )
// }

/*

DBObject 

read, write, save, id of row
unique in database?
lock row?


 */


// const users = dbpool
//     .query(userQuery)
//     .then(r => console.info(r))
//     .catch(e => console.error(e))


module.exports = { getUserDB, saveUserDB, dbpool }