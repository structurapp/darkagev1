// load all env variables and constants used throughout the app.
const dotenv = require('dotenv')

dotenv.config()

module.exports = {
    PORT: process.env.PORT,
    BASE_URL: process.env.BASE_URL,
    AUTH0_SECRET: process.env.AUTH0_SECRET,
    AUTH0_CLIENT_ID: process.env.AUTH0_CLIENT_ID,
    HMAC_KEY: process.env.HMAC_KEY,
    DEBUG: process.env.DEBUG,
    REDIS_PORT: process.env.REDIS_PORT,
    DB_USER: process.env.DB_USER,
    DB_HOST: process.env.DB_HOST,
    DB_NAME: process.env.DB_NAME,
    DB_PORT: process.env.DB_PORT,
    REDIS_PORT: process.env.REDIS_PORT,
    REDIS_HOST: process.env.REDIS_HOST,
}