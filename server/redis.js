const redis = require('redis')
const { promisify } = require('util')
const { dbpool: db } = require('./db.js')
const { REDIS_HOST, REDIS_PORT } = require('./config.js')

const redisClient = redis.createClient({
    port: REDIS_PORT,
    host: REDIS_HOST,
})

const getAsync = promisify(redisClient.get).bind(redisClient)

// redisClient.set('fromapp', 'value', (err, reply) => {
//     if (err) throw err
//     console.info(reply)
// })


// redisClient.get('fromapp', (err, reply) => {
//     if (err) throw err
//     console.info(reply)
// })

let getUserIDFromSession1 = async (sessionID) => {
    try {
        const userID = await getAsync(sessionID)
        console.log('userId from redis ', userID)
        if (!userID) {
            // we have to go grab it from DB
            return await db.query('SELECT id, name, email FROM users WHERE "email" = $1;', [sessionID])
        }
    } catch (err) {
        console.log("Something went terribly wrong fetching session id", sessionID)
        throw new Error("Session id " + sessionID + " could not be found on redis nor db, something went terribly wrong.")
    }
}

const getUserIDFromSession = async (sessionID) => {
    return await getAsync(sessionID)
}


module.exports = { redisClient, getUserIDFromSession }
