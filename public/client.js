import { draw } from './renderer.js'
import { uiKeyboardHandler, uiMouseHandler, } from './ui.js'

var HeroFactory = heroModule.HeroFactory
actionModule.genericAction()

const LOCAL_WORLDSTATE_INTERVAL = 1500 // 15 ms
const CLIENT_BROADCAST_INTERVAL = 2200 // 1s.

const params = new URLSearchParams(window.location.search)
const socket = io(
    {
        query: {
            nonce: 'value', // accessed by socket.handshake.query,
            // todo@carlo can we send something here? the client has no sensititve info.. unless
            // secret from backend.. maybe set something in redis then send it to client and read it back as socket connection gets started.
            // verify nonce in redis ?
            // this can prevent entering the world directly without going through the char selection screen.
            // todo@project when not in debug mode, user has to enter the world directly from the hero selection screen 
            // if not hero.active.
            params: params
        }
    }
)

export var Hero
let W = {} // true worldstate from server
let w = {} // local approximate worldstate
let actions = []
let processedActions = []


// init Hero with handlers.
socket.on('enterWorld', ({hero}) => {

    Hero = HeroFactory(hero)
    w.hero = Hero

    window.addEventListener('keydown', (e) => {
        Hero.keyboardHandler(e, actions)
        uiKeyboardHandler(e, actions)
    })
    window.addEventListener('mousemove', (e) => {
        Hero.mouseHandler(e, actions)
        uiMouseHandler(e, actions)
    })
})


// todo@carlo save snapshots to be processed by reconciliation
socket.on('snapshot', (trueWorldstate) => {
    W = trueWorldstate
    // console.log(`Worldstate `, W)
    // snapshots.push(trueWorldstate)
})

let FPS_INTERVAL_MS, startTime, now, then, elapsed, shouldDrawWorldstate

function startRenderLoop(fps) {
    FPS_INTERVAL_MS = 1000./fps
    console.log(`FPS interval ${FPS_INTERVAL_MS}`)
    then = Date.now()
    startTime = then
    RenderLoop()
}

// render loop has to render in under 17ms to get 60 fps. ideally under 5ms for each frame.
// the client can linearly interpolate the worldstate result between [T, T+1]
function RenderLoop() {
    requestAnimationFrame(RenderLoop)
    now = Date.now()
    elapsed = now - then
    shouldDrawWorldstate = elapsed > FPS_INTERVAL_MS
    
    if (shouldDrawWorldstate) {
        // console.log(`Hero when drawing ${JSON.stringify(Hero)}`)
        let remainder = elapsed % FPS_INTERVAL_MS
        then = now - remainder // we carry the remaining ms that did not get rendered, 
        // so then is in the past.. when elapsed: now - then 'carries over'.
        draw(w, W)
    }
}
let fps = 3
startRenderLoop(fps)

// resizeCanvas(canvas, W);

// run simulation here, sharing physics code with the server
function WorldStateLoop() {
    // console.log('Running world every ' + LOCAL_WORLDSTATE_INTERVAL + 'ms.')
    // snapshots are true worldstates w1, w2 .. wn. with timestamps
    // todo@carlo how to compute lag?
    // and interpolate based on how offsync we are accounting for lag.
    // 1. approximate local state (self.state) to server.state
    // 2.
    // 3.
    // 4.
    // 5.
    // In other words, attempts to mask network delay for the client.
    // I have to know which hero this is.
    // self.x = 
    // self.y = 
}

setInterval(WorldStateLoop, LOCAL_WORLDSTATE_INTERVAL)


function ClientBroadcastLoop() {
    if (w.hero) {
        socket.emit('action', actions)
        actions.length = 0
    }
    // for (var key in actions) delete actions[key];
    // actions.length = 0
}
setInterval(ClientBroadcastLoop, CLIENT_BROADCAST_INTERVAL)

// resize the canvas to fill browser window dynamically.
// todo@project prevent user from zooming in-out by redrawing pixels dynamically based on window zoom.
window.addEventListener('resize', () => {
        resizeCanvas(canvas, w, W)}
, false);

function resizeCanvas(canvas, w, W) {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    /**
     * drawings need to be inside this function otherwise they will be reset when 
     * you resize the browser window and the canvas will be cleared.
     */
    draw(w, W); 
}
resizeCanvas(canvas, w, W)

/*
responsibilities:
- render hero in real time.
- renders a worldstate n milliseconds in the past
- linearly interpolate between snapshots from the server
- record 'input' moves from player (in a circular buffer) and send them to the server

1. sample clock to find start time
2. sample user input
3. send user inputs using simulation time (time in the client)
4. read snapshot from server
5. determine client state 
6. render scene
7. sample clock to find end time
8. (end - start) simulation time for next frame ()

Client sees itself (hero) in present time, and other entities in the past.

Receives state from the past.

send actions in seq a1, a2, ... a_n

server responsds with index of last action processed

(W_t, a5)

client reconciliates .. how?

*/