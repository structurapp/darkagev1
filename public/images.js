// provides methods for working with images such as loading async
//
export const asyncLoadImage = async (url, w, h) => {
    return new Promise(r => {
        let img = new Image(w, h)
        img.addEventListener('load', () => {
            r(img)
        })
        img.src = url;
    })
}

