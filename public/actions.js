/*
There is a lot of behavioral richness in roguelikes.
an action is an intention that can crystallize into a concrete
worldstate
they are used by Actors. and can be contained in Items or 
standalone
On the server they are checked against other conditions
on the client they are executed instantly if they are physics-based.

let actionObj = () => {
    ref: 'object it references'
    name: ''
    ts: 'timestamp'
}
## Combat vs non-combat actions

Combat actions are pseudo-random in a predictable manner that only client+server can know 
so it can run the simulation 'deterministically' i.e. the next hit will be crit 100%
of the time if it hits, the next block will be successful always if atk < def.

so player2 blocks the supposed crit, and player1 will crit again in the next 

4,7,8,7,4,8 with longer period (ultra long period that repeats until end of universe)

Actions such as loot and crafting are probabilistic and run completely on the server.




MOVE
USE
    Inspect
    Pickpocket
    Trade
ATK
DEF

*/

(function(exports) {


function genericAction(a) {
    console.log(`Performing generic action ${a}`)
}

function MoveAction(hero, key) {
    if (key === 'd') hero.x = hero.x + 5
    if (key === 'a') hero.x = hero.x - 5
    if (key === 'w') hero.y = hero.y - 5
    if (key === 's') hero.y = hero.y + 5
    console.log(`Hero x ${hero.x}, y ${hero.y}`) // <- why doesnt this show? 
}

function BlinkAction(hero, {x, y, vx, vy}) {
    // 
}

exports.genericAction = genericAction
exports.MoveAction = MoveAction

})(typeof exports === 'undefined' ? this['actionModule'] = {}: exports)

/*

USE <> [WITH <>] [ON <>]
MOVE h.id TO x,y
MOVE h.id TOWARDS dx, dy [TOWARDS VEL vx, vy] 
TRADE h1.id <> WITH h2.id <...><>
INSPECT
PUT, PATCH, GET?



MISSILE AREA ATK 10 DMG 20

BUFF 20HP
DOT 
AOE 
DIRECT ATK 

COMBAT

Attacks can be direct. 
Attacks are AREAS with dt given in frames. we do collision checking
a fireball is moving vx, vy and has a lifespan of 5s (300 frames)
a sword swing is a static box lasting 0.5 seconds 30frames.
any time it collides it 'hits'.
A sword CAN hit multiple times, this way you will take full damage if you stay in range.



'Hits' have a mechanism for resolution.

*/