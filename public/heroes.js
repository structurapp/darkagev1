//
console.log('Loaded via pug.')
// window.location.href = '/index'
// window.location.assign('/index')

async function createHero(data={name, race, anon}) {
  const payload = JSON.stringify(data)
  console.log('Sending ...', payload)
  const response = await fetch('/v2/createHero', {
      method: 'POST', // *GET, POST, PUT, DELETE, etc.
      body: payload, // body data type must match "Content-Type" header
      mode: 'cors', // no-cors, *cors, same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow', // manual, *follow, error
      referrerPolicy: 'no-referrer' // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
  })
  return response.json()
}
