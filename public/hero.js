//
(function(exports) {

    let client = Object.keys(this).indexOf("actionModule") !== -1 // false in CommonJS i.e node
    // because window does not have actionModule populated

    if (client) {
        console.log('Loading actions from the client')
        var genericAction = actionModule.genericAction
        var MoveAction = actionModule.MoveAction
    } else {
        console.log('Loading actions from the server')
        var { genericAction, MoveAction } = require('./actions.js')
    }

function HeroFactory({
    id, user_id, name, race, sprite, alive, playing, hp, atk, def, gold, x, y, vx, vy
}={}) {
    console.log(`Hero ${id} created on the client\n`)
    return {
        id, user_id, name, race, sprite, alive, playing, hp, atk, def, gold, x, y, vx, vy,
        // this.keyboardHandler2 = function(e) {
        //     console.log(`Inside the heroKbd handler 2 pressing ${e.key}`)
        // },
        keyboardHandler: function(e, actions) {
                let key = e.key
                if ('wasd'.indexOf(key) !== -1) MoveAction(this, key)
                actions.push(key)
        },

        mouseHandler: (e) => {
            // todo@carlo how to show info
            // when hovering over

        },
        // combat & non combat logs.
        // Bat flees in terror
        // Goblin archer is weak
        // Goblin spearman impales you in the head brutally. you die. 
        showLogs: () =>{}
    }
}



exports.HeroFactory = HeroFactory

})(typeof exports === 'undefined' ? this['heroModule'] = {}: exports)


// let { Tile } = require('./ecs')
// let { Action } = require('./actions')

// const Hero = ({
//     id, user_id, x, y, vx, vy, name, race, hp, atk, def, gold=0,
// }) => ({
//     id, user_id, x, y, vx, vy, name, race, hp, atk, def, gold,
//     // getHP: () => this.hp
// })

// function Hero({id, x, y}) {
//     id, x, y
// }

// export function HeroFactory(...o) { 

//     console.log("herofactory being called")
    
//     Object.assign({},
//     // Tile(...o),
//     // Actor(...o),
//     // Fighter(...o),
//     Hero(...o)
// )
