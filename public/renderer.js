import { asyncLoadImage } from './images.js'
// const background = new Image()
// background.src = "world_background.png"
import { randomInteger } from './utils.js'

var canvas = document.querySelector("canvas")
var ctx = canvas.getContext("2d")

let sprites
let map
asyncLoadImage('./sprites.jpg')
    .then(img => {sprites = img})
    .catch(e => console.log('Could not load sprites!, ' + JSON.stringify(e)))


asyncLoadImage('./forest-big.jpg', 3840, 2133)
    .then(img => {map = img})
    .catch(e => console.log('Could not load map!, ' + JSON.stringify(e)))



function centeredTile({x, y, sprite}) {
    // o is the origin
    // void ctx.drawImage(image, ox, oy, sWidth, sHeight, dx, dy, dWidth, dHeight);
    // vec = (ox, oy, dx, dy)
    // drawImage(sourceVec, DestinationVec)
    let wx = window.innerWidth
    let wy = window.innerHeight
    let spriteWidth = sprite.slice(2, 3)[0]
    let spriteHeight = sprite.slice(3, 4)[0]
    let ox = 0.5*(wx - spriteWidth)
    let oy = 0.5*(wy - spriteHeight)

    let pos = sprite.concat([ox, oy, spriteWidth, spriteHeight])

    return pos
}

function relativeCenteredPosition(tile, X, Y) {
    let wx = window.innerWidth
    let wy = window.innerHeight
    let {sprite, x, y} = tile
    let DX = X - x
    let DY = Y - y
    let dx = sprite.slice(2, 3)[0]
    let dy = sprite.slice(3, 4)[0]
    let ox = 0.5*(wx - DX - dx)
    let oy = 0.5*(wy - DY - dy)
    
    let pos = sprite.concat([ox, oy, dx, dy])
    return pos
}

function cropMap(map, x, y) {
    let pos


    return pox

}



// takes in Worldstate and draws it on global canvas
export function draw({hero}, {heroes, mobs, serverHero}) {
    // console.log(`Heroes inside draw ${JSON.stringify(heroes)} `)
    // console.log(`Self inside draw ${JSON.stringify(self)} `)
    let heroPos
    let positions = []
    let wx = window.innerWidth
    let wy = window.innerHeight

    ctx.clearRect(0, 0, wx, wy)
    
    if (hero) {
        let pos = centeredTile(hero)
        heroPos = pos
        positions.push(pos)
        if (map) {
            let positionMap = cropMap(map, hero.x, hero.y)
            ctx.drawImage(map, ...positionMap)
        }
    }

    if (heroes) {
        for (let j in heroes) {
            let other = heroes[j]
            let pos = relativeCenteredPosition(other, hero.x, hero.y)
            positions.push(pos)
        }
    }

    for (let i in positions) {
        let pos = positions[i]
        ctx.drawImage(sprites, ...pos)
    }
}